package com.tolgas.restapicoveragebackend.model.document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestResponse {
    private Integer code;
    private Object body;
}
