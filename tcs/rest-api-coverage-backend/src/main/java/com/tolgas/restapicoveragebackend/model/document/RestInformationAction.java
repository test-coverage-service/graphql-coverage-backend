package com.tolgas.restapicoveragebackend.model.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document
@AllArgsConstructor
public class RestInformationAction {
    @MongoId
    private Long id;
    private RestRequest request;
    private RestResponse response;
}
