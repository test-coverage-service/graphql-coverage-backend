package com.tolgas.restapicoveragebackend.model.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.RequestEntity;

import java.net.URI;

@Data
@AllArgsConstructor
public class RestRequest {
    private String path;
    private Object body;

    private HttpType httpType;

    public RequestEntity<?> toRequestEntity() {
        return switch (httpType) {
            case POST -> RequestEntity.post(URI.create(path)).body(body);
            case PUT -> RequestEntity.put(URI.create(path)).body(body);
            case PATCH -> RequestEntity.patch(URI.create(path)).body(body);
            case DELETE -> RequestEntity.delete(URI.create(path)).build();
            case GET -> RequestEntity.get(URI.create(path)).build();
        };
    }

    public enum HttpType {
        POST, PUT, PATCH, DELETE, GET
    }
}
