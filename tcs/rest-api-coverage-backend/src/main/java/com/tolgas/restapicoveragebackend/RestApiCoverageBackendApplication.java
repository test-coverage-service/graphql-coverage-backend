package com.tolgas.restapicoveragebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiCoverageBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestApiCoverageBackendApplication.class, args);
    }

}
