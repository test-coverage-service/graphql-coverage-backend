package com.tolgas.restapicoveragebackend.api;

import com.tolgas.restapicoveragebackend.model.document.RestInformationAction;
import com.tolgas.restapicoveragebackend.repository.RestInformationActionRepository;
import com.tolgas.restapicoveragebackend.service.RestInformationActionProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RestActionController {

    @Autowired
    RestInformationActionRepository restInformationActionRepository;

    @Autowired
    RestInformationActionProcessor restInformationActionProcessor;

    @PostMapping("/api/v1/rest-action")
    public RestInformationAction create(@RequestBody RestInformationAction informationProcessDocument) {
        return restInformationActionRepository.save(informationProcessDocument);
    }

    @GetMapping("/api/v1/rest-action/{id}")
    public RestInformationAction get(@PathVariable Long id) {
        return restInformationActionRepository.findById(id).get();
    }

    @DeleteMapping("/api/v1/rest-action/{id}")
    public void delete(@PathVariable Long id) {
        restInformationActionRepository.deleteById(id);
    }

    @PutMapping("/api/v1/rest-action/{id}")
    public RestInformationAction update(@PathVariable Long id,
                                             @RequestBody RestInformationAction informationProcessDocument) {
        informationProcessDocument.setId(id);
        return restInformationActionRepository.save(informationProcessDocument);
    }

    @PostMapping("/api/v1/rest-action/{id}/process")
    public void process(@PathVariable Long id) {
        restInformationActionProcessor.process(id);
    }
}
