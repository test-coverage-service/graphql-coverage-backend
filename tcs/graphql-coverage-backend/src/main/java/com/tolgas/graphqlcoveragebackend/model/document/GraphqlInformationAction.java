package com.tolgas.graphqlcoveragebackend.model.document;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document
@NoArgsConstructor
public class GraphqlInformationAction {
    @MongoId
    private Long id;
    private GraphqlRequest graphqlRequest;
    private GraphqlResponse graphqlResponse;
}
