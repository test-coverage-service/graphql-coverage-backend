package com.tolgas.graphqlcoveragebackend.model.document;

public class GraphqlResponse {
    private String status;
    private Object body;

    public GraphqlResponse(String status, Object body) {
        this.status = status;
        this.body = body;
    }

    public GraphqlResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
