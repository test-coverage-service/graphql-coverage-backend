package com.tolgas.graphqlcoveragebackend.repository;

import com.tolgas.graphqlcoveragebackend.model.document.GraphqlInformationAction;
import org.springframework.data.repository.CrudRepository;

public interface GraphqlInformationActionRepository  extends CrudRepository<GraphqlInformationAction, Long> {
}
