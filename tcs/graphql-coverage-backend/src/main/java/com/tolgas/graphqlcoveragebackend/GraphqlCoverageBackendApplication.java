package com.tolgas.graphqlcoveragebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlCoverageBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraphqlCoverageBackendApplication.class, args);
    }

}
