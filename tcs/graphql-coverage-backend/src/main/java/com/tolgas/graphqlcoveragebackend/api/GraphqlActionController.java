package com.tolgas.graphqlcoveragebackend.api;

import com.tolgas.graphqlcoveragebackend.model.document.GraphqlInformationAction;
import com.tolgas.graphqlcoveragebackend.repository.GraphqlInformationActionRepository;
import com.tolgas.graphqlcoveragebackend.service.GraphqlInformationActionProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GraphqlActionController {

    @Autowired
    GraphqlInformationActionRepository graphqlInformationActionRepository;

    @Autowired
    GraphqlInformationActionProcessor graphqlInformationActionProcessor;

    @PostMapping("/api/v1/graphql-action")
    public GraphqlInformationAction create(@RequestBody GraphqlInformationAction graphqlInformationAction) {
        return graphqlInformationActionRepository.save(graphqlInformationAction);
    }

    @GetMapping("/api/v1/graphql-action/{id}")
    public GraphqlInformationAction get(@PathVariable Long id) {
        return graphqlInformationActionRepository.findById(id).get();
    }

    @DeleteMapping("/api/v1/graphql-action/{id}")
    public void delete(@PathVariable Long id) {
        graphqlInformationActionRepository.deleteById(id);
    }

    @PutMapping("/api/v1/graphql-action/{id}")
    public GraphqlInformationAction update(@PathVariable Long id,
                                             @RequestBody GraphqlInformationAction graphAction) {
        graphAction.setId(id);
        return graphqlInformationActionRepository.save(graphAction);
    }

    @PostMapping("/api/v1/graphql-action/{id}/process")
    public void process(@PathVariable Long id) {
        graphqlInformationActionProcessor.process(id);
    }
}
