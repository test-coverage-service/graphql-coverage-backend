package com.tolgas.graphqlcoveragebackend.service;

import com.tolgas.graphqlcoveragebackend.model.document.GraphqlInformationAction;
import com.tolgas.graphqlcoveragebackend.model.document.GraphqlRequest;
import com.tolgas.graphqlcoveragebackend.repository.GraphqlInformationActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class GraphqlInformationActionProcessor {
    @Autowired
    private GraphqlInformationActionRepository graphqlInformationActionRepository;

    RestTemplate restTemplate;

    private Logger logger;

    public void process(Long id) {
        logger.log(Level.INFO, "Starting process for GraphQl information action...");

        GraphqlInformationAction action = graphqlInformationActionRepository.findById(id).get();

        GraphqlRequest request = action.getGraphqlRequest();

        Object result = restTemplate.postForEntity(request.getQuery(), request.getVariables(), Object.class);
        action.getGraphqlResponse().setBody(result);

        graphqlInformationActionRepository.save(action);
    }
}
