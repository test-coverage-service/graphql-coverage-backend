package com.tolgas.graphqlcoveragebackend.model.document;

import java.util.List;

public class GraphqlRequest {
    private String query;
    private List<String> variables;

    public GraphqlRequest(String query, List<String> variables) {
        this.query = query;
        this.variables = variables;
    }

    public GraphqlRequest() {
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }
}
