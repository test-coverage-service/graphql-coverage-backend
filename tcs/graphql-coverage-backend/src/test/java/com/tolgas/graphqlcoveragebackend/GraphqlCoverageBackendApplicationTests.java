package com.tolgas.graphqlcoveragebackend;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.tolgas.graphqlcoveragebackend.api.GraphqlActionController;
import com.tolgas.graphqlcoveragebackend.model.document.GraphqlInformationAction;
import com.tolgas.graphqlcoveragebackend.model.document.GraphqlRequest;
import com.tolgas.graphqlcoveragebackend.model.document.GraphqlResponse;
import com.tolgas.graphqlcoveragebackend.repository.GraphqlInformationActionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GraphqlCoverageBackendApplicationTests {

    @Autowired
    GraphqlActionController actionController;

    @Autowired
    GraphqlInformationActionRepository graphqlInformationActionRepository;

    WireMock serverStub = new WireMock();

    @Test
    void contextLoads() {
        GraphqlInformationAction testingAction = new GraphqlInformationAction();
        testingAction.setId(1L);
        GraphqlRequest request = new GraphqlRequest();
        request.setQuery("createUser($name) {name}");
        request.setVariables(Arrays.asList("Vasya"));
        GraphqlResponse response = new GraphqlResponse();
        testingAction.setGraphqlRequest(request);
        testingAction.setGraphqlResponse(response);
        graphqlInformationActionRepository.save(testingAction);


        StubMapping responseValid = stubFor(
                post(urlEqualTo("/api/v1/system"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse().withStatus(200)
                        .withHeader("Content-Type", "application/json").withBody(response.getBody().toString())));

        actionController.process(1L);

        assertEquals(1, serverStub.getServeEvents().size());
    }

}
