package com.tolgas.tcs.base.service;

public interface InformationActionProcessor<A> {
    void process(A action);
}
