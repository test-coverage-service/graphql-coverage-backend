package com.tolgas.tcs.informationprocessorbackend.model.document;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.List;

@Data
@Document
public class InformationProcessDocument {
    @MongoId
    private Long id;
    List<InformationActionDocument> actions = new ArrayList<>();
}
